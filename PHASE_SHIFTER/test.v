`timescale 1ps/1ps

module test ;
   //input
   reg          clk ;
   reg          rst ;
   reg          en ;
   reg [15:0]   init_phase;
   reg          init_phase_valid;
   reg [15:0]   freq_word;

   //output
   wire         valid ;
   wire [12:0]  yout ;
   wire [13:0]  phase_out;
   wire         phase_valid;

always #5 clk <= ~clk;

initial begin
   #0 begin rst <= 1; clk <=0; en <= 0; init_phase_valid <=0;init_phase <= 16'd16384;freq_word <= 16'd1024;end
   #50 begin rst <= 0; end
   #20 begin en <= 1;end
   #2000 begin init_phase_valid <= 1;end
   #10 begin init_phase_valid <=0;end
   #40000 $finish();

end

initial
begin            
    $dumpfile("wave.vcd");        //生成的vcd文件名称
    $dumpvars(0, test);    //tb模块名称
end

nco_gen #(
   .DATA_WIDTH(13),
   .DATA_COUNT(1024),
   .DATA_COUNT_LOG2(10),
   .PHASE_WIDTH(16)
) nco_dut (
   .sys_clk (clk),
   .sys_rst (rst),
   .enable  (en),
   .init_phase (init_phase),
   .init_phase_valid (init_phase_valid),
   .freq_word (freq_word),

   .out_data (yout),
   .valid    (valid)
);

phase_shifter #(
   .DATA_WIDTH(13),
   .RAM_ADDR_FULL(2048),
   .RAM_ADDR_WIDTH(11)
) dut
(
   .sys_clk(clk),
   .sys_rst(rst),
 
   .in_data(yout),
   .in_strobe(valid),
 
   .in_phase(256),
   .full_address(725),
   .phase_valid(1'b1),

   .out_data(phase_out),
   .out_valid(phase_valid)
);

endmodule