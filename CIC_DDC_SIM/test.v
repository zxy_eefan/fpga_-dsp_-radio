`timescale 1ps/1ps

//This File modified from https://www.runoob.com/w3cnote/verilog-cic.html

module test ;
   //input
   reg          clk ;
   reg          rst_n ;
   reg          en ;
   reg [11:0]   xin ;
   //output
   wire         valid ;
   wire [11:0]  yout ;

   parameter    SIMU_CYCLE   = 64'd2000 ;
   parameter    SIN_DATA_NUM = 200 ;
//=====================================
// 50MHz clk generating
   localparam   TCLK_HALF     = 10_000;
   initial begin
      clk = 1'b0 ;
      forever begin
         # TCLK_HALF ;
         clk = ~clk ;
      end
   end

//============================
//  reset and finish

   initial begin
      rst_n = 1'b0 ;
      # 40_000 ;
      rst_n = 1'b1 ;
      # (TCLK_HALF * 2 * SIMU_CYCLE) ;
      $finish ;
   end

//=======================================
// read sinx.txt to get sin data into register
   reg          [11:0] stimulus [0: SIN_DATA_NUM-1] ;
   integer      i ;
   initial begin
      $readmemh("cosx0p25m7p5m12bit.txt", stimulus) ;
      i = 0 ;
      en = 0 ;
      xin = 0 ;
      # 200 ;
      forever begin
         @(negedge clk) begin
            en          = 1'b1 ;
            xin         = stimulus[i] ;
            if (i == SIN_DATA_NUM-1) begin
               i = 0 ;
            end
            else begin
               i = i + 1 ;
            end
         end
      end // forever begin
   end // initial begin

//Generate signed signal
wire signed [11:0] data_in;
assign data_in = xin - 12'd2048;

jerrytech_cic cic_dut(
    .sys_clk (clk),
    .sys_rst (~rst_n),

    .data_in (data_in),
    .enable(en),

    .data_out(yout),
    .valid(valid)
);

//Use for iverilog and gtkwave
initial
begin            
    $dumpfile("wave.vcd");        //生成的vcd文件名称
    $dumpvars(0, test);    //tb模块名称
end

endmodule // test
