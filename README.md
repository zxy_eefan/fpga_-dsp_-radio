# FPGA_DSP_Radio

#### 介绍
基于FPGA的DSP/无线电开源IP仓库
仿真采用iverilog + GTKwave开源工具链

双击每个IP目录的bat可综合仿真


#### 版权声明

所有IP源码均为原创，测试testbench部分来源为网络上的开源项目，如有侵权请告知删除。


