`timescale 1ps/1ps

module test ;
   //input
   reg          clk ;
   reg          rst ;
   reg          en ;
   reg [15:0]   init_phase;
   reg          init_phase_valid;
   reg [15:0]   freq_word;

   //output
   wire         valid_i ;
   wire [12:0]  i_data ;
   wire         valid_q ;
   wire [12:0]  q_data ;
   wire [26:0]  duc_o;
   wire         duc_valid;
   wire [25:0]  i_ddc_data;
   wire [25:0]  q_ddc_data;
   wire         ddc_valid;


always #5 clk <= ~clk;

initial begin
   #0 begin rst <= 1; clk <=0; en <= 0; init_phase_valid <=0;end
   #50 begin rst <= 0; end
   #20 begin en <= 1;end
   #2000 begin init_phase_valid <= 1;end
   #10 begin init_phase_valid <=0;end
   #40000 $finish();

end

initial
begin            
    $dumpfile("wave.vcd");        //生成的vcd文件名称
    $dumpvars(0, test);    //tb模块名称
end

nco_gen #(
   .DATA_WIDTH(13),
   .DATA_COUNT(1024),
   .DATA_COUNT_LOG2(10),
   .PHASE_WIDTH(16),
   .INIT_PHASE(16'd16384)
) nco_i (
   .sys_clk (clk),
   .sys_rst (rst),
   .enable  (en),
   .freq_word (16'd128),

   .out_data (i_data),
   .valid    (valid_i)
);

nco_gen #(
   .DATA_WIDTH(13),
   .DATA_COUNT(1024),
   .DATA_COUNT_LOG2(10),
   .PHASE_WIDTH(16),
   .INIT_PHASE(0)
) nco_q (
   .sys_clk (clk),
   .sys_rst (rst),
   .enable  (en),
   .freq_word (16'd128),

   .out_data (q_data),
   .valid    (valid_q)
);

digital_mixer # (
   .I_WIDTH(13),
   .Q_WIDTH(13),
   .NCO_WIDTH(13),
   .PHASE_WIDTH(16)
) tx_mixer_dut
(
   .sys_clk(clk),
   .sys_rst(rst),
   .enable(en),
   .i_data(i_data),
   .q_data(q_data),
   .freq_word(16'd2048),
   .o_data(duc_o),
   .valid(duc_valid)
);

digital_mixer_rx # (
   .IN_WIDTH(13),
   .NCO_WIDTH(13),
   .PHASE_WIDTH(16)
) rx_mixer_dut
(
   .sys_clk(clk),
   .sys_rst(rst),
   .enable(en),
   .i_data(i_ddc_data),
   .q_data(q_ddc_data),
   .freq_word(16'd2048),
   .in_data(duc_o[26:14]),
   .valid(ddc_valid)
);


endmodule