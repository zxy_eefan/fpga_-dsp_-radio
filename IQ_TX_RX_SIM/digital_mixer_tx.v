module digital_mixer # (
    parameter I_WIDTH = 13,
    parameter Q_WIDTH = 13,
    parameter NCO_WIDTH = 13,
    parameter PHASE_WIDTH = 16
)
(
    input wire                                  sys_clk,
    input wire                                  sys_rst,
    input wire                                  enable,
    input wire signed   [I_WIDTH - 1       :0 ] i_data,
    input wire signed   [Q_WIDTH - 1       :0 ] q_data,
    input wire          [PHASE_WIDTH - 1   :0 ] freq_word,
    output reg signed   [I_WIDTH + Q_WIDTH :0 ] o_data,
    output reg                                  valid
);

wire signed [NCO_WIDTH - 1 :0 ] cos_wave;
wire signed [NCO_WIDTH - 1 :0 ] sin_wave;
wire signed [NCO_WIDTH + I_WIDTH - 1:0 ] i_ch_data;
wire signed [NCO_WIDTH + Q_WIDTH - 1:0 ] q_ch_data;

wire cos_valid;
wire sin_valid;
wire i_ch_valid;
wire q_ch_valid;

//I channel cos
nco_gen #(
   .DATA_WIDTH(NCO_WIDTH),
   .DATA_COUNT(1024),
   .DATA_COUNT_LOG2(10),
   .PHASE_WIDTH(PHASE_WIDTH),
   .INIT_PHASE({1'd1,1'b1,{(PHASE_WIDTH-2){1'b0}}})
) nco_i (
   .sys_clk (sys_clk),
   .sys_rst (sys_rst),
   .enable  (enable),
   .freq_word (freq_word),

   .out_data (cos_wave),
   .valid    (cos_valid)
);

//I channel mult
mult_sim #(
   .INPUT_WIDTH_A(NCO_WIDTH),
   .INPUT_WIDTH_B(I_WIDTH),
   .OUTPUT_WIDTH(NCO_WIDTH + I_WIDTH),
   .DELAY(3)
) mult_i
(
   .sys_clk(sys_clk),
   .sys_rst(sys_rst),
   .enable(cos_valid),
   .data_a(cos_wave),
   .data_b(i_data),
   .data_o(i_ch_data),
   .valid(i_ch_valid)
);

//Q channel sin
nco_gen #(
   .DATA_WIDTH(NCO_WIDTH),
   .DATA_COUNT(1024),
   .DATA_COUNT_LOG2(10),
   .PHASE_WIDTH(PHASE_WIDTH),
   .INIT_PHASE({1'd1,1'b0,{(PHASE_WIDTH-2){1'b0}}})
) nco_q (
   .sys_clk (sys_clk),
   .sys_rst (sys_rst),
   .enable  (enable),
   .freq_word (freq_word),

   .out_data (sin_wave),
   .valid    (sin_valid)
);

//Q channel mult
mult_sim #(
   .INPUT_WIDTH_A(NCO_WIDTH),
   .INPUT_WIDTH_B(Q_WIDTH),
   .OUTPUT_WIDTH(NCO_WIDTH + Q_WIDTH),
   .DELAY(3)
) mult_q
(
   .sys_clk(sys_clk),
   .sys_rst(sys_rst),
   .enable(sin_valid),
   .data_a(sin_wave),
   .data_b(q_data),
   .data_o(q_ch_data),
   .valid(q_ch_valid)
);

always @ (posedge sys_clk)
    if(sys_rst)
        o_data <= 0;
    else if(q_ch_valid && i_ch_valid)
        o_data <= i_ch_data - q_ch_data; 

always @ (posedge sys_clk)
    if(sys_rst)
        valid <= 0;
    else
        valid <= q_ch_valid && i_ch_valid;

endmodule
