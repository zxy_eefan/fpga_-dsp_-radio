`timescale 1ps/1ps

module test ;
   //input
   reg          clk ;
   reg          rst_n ;
   reg          en ;
   reg [11:0]   xin ;
   //output
   wire         valid ;
   wire [23:0]  yout ;

   parameter    SIMU_CYCLE   = 64'd2000 ;
   parameter    SIN_DATA_NUM = 1024 ;
//=====================================
// 50MHz clk generating
   localparam   TCLK_HALF     = 10_000;
   initial begin
      clk = 1'b0 ;
      forever begin
         # TCLK_HALF ;
         clk = ~clk ;
      end
   end

//============================
//  reset and finish

   initial begin
      rst_n = 1'b0 ;
      # 40_000 ;
      rst_n = 1'b1 ;
      # (TCLK_HALF * 2 * SIMU_CYCLE) ;
      $finish ;
   end

//=======================================
// read sinx.txt to get sin data into register
   reg          [11:0] stimulus [0: SIN_DATA_NUM-1] ;
   reg          [10:0] cnt;
   integer      i ;
   initial begin
      $readmemh("sine.txt", stimulus) ;
      i = 0 ;
      en = 0 ;
      xin = 0 ;
      cnt = 0 ;
      # 200 ;
      forever begin
         @(negedge clk) begin
            en          = 1'b0 ;
            xin         = stimulus[i] ;
            if (i >= SIN_DATA_NUM-32 && cnt == 11) begin
               i = 0 ;
               en = 1;
            end
            else if(cnt == 11)begin
               i = i + 32 ;
               en = 1;
            end
            else
               en = 0;
            cnt = cnt == 11 ? 0 : cnt + 1;
         end
      end // forever begin
   end // initial begin

wire signed [11:0] data_in;
assign data_in = xin - 12'd2048;


jerrytech_cic_insert cic_insert_dut(
    .sys_clk (clk),
    .sys_rst (~rst_n),

    .data_in (data_in),
    .enable(en),

    .data_out(yout),
    .valid(valid)
);

initial
begin            
    $dumpfile("wave.vcd");        //生成的vcd文件名称
    $dumpvars(0, test);    //tb模块名称
end

endmodule // test
