module mult_sim #(
    parameter INPUT_WIDTH_A = 12,
    parameter INPUT_WIDTH_B = 12,
    parameter OUTPUT_WIDTH  = 24,
    parameter DELAY         = 2
)
(
    input  wire                               sys_clk,
    input  wire                               sys_rst,
    input  wire                               enable,
    input  wire signed [INPUT_WIDTH_A - 1:0 ] data_a,
    input  wire signed [INPUT_WIDTH_B - 1:0 ] data_b,
    output wire signed [OUTPUT_WIDTH  - 1:0 ] data_o,
    output wire                               valid
);

reg [DELAY - 1 :0 ] delay_reg;
reg signed [OUTPUT_WIDTH  - 1:0 ] mult_reg [0 : DELAY - 1];
reg [10:0 ] i;

always @ (posedge sys_clk)
    if(sys_rst)
        delay_reg <= 0;
    else if(enable) begin
        delay_reg[0] <= enable;
        if(DELAY - 1 > 0)
            delay_reg[DELAY - 1 :1 ] <= {delay_reg[DELAY - 2 :1 ],delay_reg[0]};
    end

assign valid = delay_reg[DELAY - 1];

always @ (posedge sys_clk)
    if(sys_rst)
        for(i=0;i<DELAY;i=i+1) begin
            mult_reg[i] <= 0;
        end
    else if(enable) begin
        mult_reg[0] <= {{(OUTPUT_WIDTH - INPUT_WIDTH_A){data_a[INPUT_WIDTH_A - 1]}},data_a} * {{(OUTPUT_WIDTH - INPUT_WIDTH_B){data_b[INPUT_WIDTH_B - 1]}},data_b};
        for(i=1;i<DELAY;i=i+1)
            mult_reg[i] <= mult_reg[i - 1];
    end

assign data_o = mult_reg[DELAY - 1];

endmodule
