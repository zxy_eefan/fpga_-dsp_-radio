`timescale 1ps/1ps

module test ;
   //input
   reg          clk ;
   reg          rst ;
   reg          en ;
   reg [15:0]   init_phase;
   reg          init_phase_valid;
   reg [15:0]   freq_word;

   //output
   wire         valid0 ;
   wire [12:0]  yout0 ;
   wire         valid1 ;
   wire [12:0]  yout1 ;
   wire [25:0]  mult_o;
   wire         mult_valid;

always #5 clk <= ~clk;

initial begin
   #0 begin rst <= 1; clk <=0; en <= 0; init_phase_valid <=0;init_phase <= 16'd16384;end
   #50 begin rst <= 0; end
   #20 begin en <= 1;end
   // #2000 begin init_phase_valid <= 1;end
   #10 begin init_phase_valid <=0;end
   #40000 $finish();

end

initial
begin            
    $dumpfile("wave.vcd");        //生成的vcd文件名称
    $dumpvars(0, test);    //tb模块名称
end

nco_gen #(
   .DATA_WIDTH(13),
   .DATA_COUNT(1024),
   .DATA_COUNT_LOG2(10),
   .PHASE_WIDTH(16)
) nco_lowfreq (
   .sys_clk (clk),
   .sys_rst (rst),
   .enable  (en),
   .init_phase (init_phase),
   .init_phase_valid (init_phase_valid),
   .freq_word (16'd512),

   .out_data (yout0),
   .valid    (valid0)
);

nco_gen #(
   .DATA_WIDTH(13),
   .DATA_COUNT(1024),
   .DATA_COUNT_LOG2(10),
   .PHASE_WIDTH(16)
) nco_highfreq (
   .sys_clk (clk),
   .sys_rst (rst),
   .enable  (en),
   .init_phase (init_phase),
   .init_phase_valid (init_phase_valid),
   .freq_word (16'd8192),

   .out_data (yout1),
   .valid    (valid1)
);

mult_sim #(
   .INPUT_WIDTH_A(13),
   .INPUT_WIDTH_B(13),
   .OUTPUT_WIDTH(26),
   .DELAY(3)
) mult_dut
(
   .sys_clk(clk),
   .sys_rst(rst),
   .enable(valid1 && valid0),
   .data_a(yout0),
   .data_b(yout1),
   .data_o(mult_o),
   .valid(mult_valid)
);

endmodule